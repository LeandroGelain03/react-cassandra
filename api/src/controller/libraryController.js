const cassandra = require('cassandra-driver');

const client = new cassandra.Client({ contactPoints: ['localhost'], localDataCenter: 'datacenter1', keyspace:"library" },)

module.exports = {

  async store(req, res) {
    const { name_book, author_name, gender, sinopse } = req.body;

    const id = await client.execute(`SELECT id FROM library.libraries;`)
    var bigestID = 0
    id.rows.map( row => {
      if (row.id >= bigestID){
        bigestID = row.id
      }
    })
    await client.execute(`INSERT INTO library.libraries (id, name_book, author_name, gender, sinopse) values (?,?,?,?,?);`,
      [bigestID+1, name_book, author_name, gender, sinopse], { prepare : true }, (err, callback) => {
        if (err) {
          return res.status(500).send(err)
        }
        return res.status(200).send("Livro cadastrado")
      }
    )
  },
  async edit(req, res) {
    const { name_book, author_name, gender, sinopse } = req.body;
    const { id } = req.query;
    console.log(id)
    await client.execute(`UPDATE library.libraries SET name_book=?, author_name=?, gender=?, sinopse=? WHERE id=?;`,
      [ name_book, author_name, gender, sinopse, id], { prepare : true }, (err, callback) => {
        if (err) {
          return res.status(500).send(err)
        }
        return res.status(200).send("Livro editado")
      }
    )
  },
  async delete(req, res) {
    const { id } = req.body;
    await client.execute(`DELETE FROM library.libraries WHERE id=?;`,
      [id], { prepare : true }, (err, callback) => {
        if (err) {
          return res.status(500).send(err)
        }
        return res.status(200).send("Livro deletado")
      }
    )
  },
  async show(req, res) {
    await client.execute(`SELECT * FROM library.libraries;`,[],
      { prepare : true }, (err, callback) => {
        if (err) {
          console.log(err)
          return res.status(500).send(err)
        }
        return res.status(200).send(callback.rows)
      }
    )
  },
}
